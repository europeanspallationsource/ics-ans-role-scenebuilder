ics-ans-role-scenebuilder
===================

Ansible role to install scenebuilder in the Development Machine.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
scenebuilder_version: 8.4.1
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-scenebuilder
```

License
-------

BSD 2-clause
