import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_scenebuilder_install_complete(host):
    for package in ('scenebuilder', 'gnome-classic-session'):
        assert host.package(package).is_installed
    assert host.package("scenebuilder").version == '8.4.1'


def test_scenebuilder_binary_checksum(host):
    assert host.file("/opt/SceneBuilder/SceneBuilder").sha256sum == 'a2216ec1be1b86e5aafe96981db69ab5f65839487cddaf1feda5d46ede718d51'
